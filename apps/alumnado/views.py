# -*- encoding: utf-8 -*-
from django.http import HttpResponse
from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.alumnado.serializers import AlumnoSerializers
from apps.alumnado.services import AlumnoServices, MateriaServices, MaestroServices
from .forms import AlumnoForm
from apps.alumnado.models import Materia, Maestro, Alumno
from django.shortcuts import redirect
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic



# Create your views here.
# funcion para crear una materia
# def crear_materia(request):
#     if request.method == 'POST':
#         form = MateriaForm(request.POST)
#         if form.is_valid():
#             form.save()
#             return redirect('home')
#     else:
#         form = MateriaForm()
#     return render(request, 'alumnado/crear_materia.html',{'form':form})

class SignUp(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'registration/signup.html'


def alumnado_vista(request):
    return render(request, 'alumnado/alumnado.html')


def alumno_lista(request):
    alumnos = Alumno.objects.filter(disponible = True)
    return render(request, 'alumnado/alumnos_lista.html')
    # return render(request, 'alumnado/alumnos_lista.html', {'alumnos': alumnos})


class AlumnoApi(APIView):
    def get(self, request):
        return Response(AlumnoServices.get_list())

    def post(self, request):
        response_data = {}
        try:
            response_data['success'] = AlumnoServices.guardar(request.data)
            response_data['message'] = 'El alumno se ha guardado'
        except Exception as e:
            response_data['success'] = False
            response_data['error'] = str(e)
        return Response(response_data)

    def delete(self, request):
        response_data = {}
        try:
            response_data['success'] = AlumnoServices.inactive(request.data)
            response_data['message'] = 'El alumno se ha eliminado'
        except Exception as e:
            response_data['success'] = False
            response_data['error'] = str(e)
        return Response(response_data)

def alumno_edit(request, pk):
    alumno = Alumno.objects.get(pk=pk)
    if request.method == "POST":
        form = AlumnoForm(request.POST, instance=alumno)
        if form.is_valid():
            alumno = form.save(commit=False)
            alumno.save()
            return redirect('alumno_lista')
    else:
        form = AlumnoForm(instance=alumno)
    return render(request, 'alumnado/alumnos_edit.html', {'form': form})


def alumno_delete(request, pk):
    alumno = Alumno.objects.get(pk=pk)
    print (alumno.disponible)
    alumno.disponible = not alumno.disponible
    print (alumno.disponible)
    alumno.save()
    # alumno.delete()
    return redirect('alumno_lista')


class MateriaSearchApi(APIView):
    def get(self, request):
        term = request.GET.get('term[term]', '')
        if term != '':
            print(term)
        return HttpResponse(MateriaServices.get_options(term=term), 'applications/json')


class MaestroSearchApi(APIView):
    def get(self, request):
        term = request.GET.get('term[term]', '')
        if term != '':
            print(term)
        return HttpResponse(MaestroServices.get_options(term=term), 'applications/json')
