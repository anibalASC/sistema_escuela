import json

from rest_framework import serializers
from datetime import date
from .models import Alumno

class AlumnoSerializers(serializers.ModelSerializer):
    age = serializers.SerializerMethodField()
    maestro_name = serializers.SerializerMethodField()
    materia_name = serializers.SerializerMethodField()
    maestro_json = serializers.SerializerMethodField()
    materia_json = serializers.SerializerMethodField()

    def get_age(self,obj):
        try:
            today = date.today()
            born = obj.fecha_nacimiento_alumno
            return today.year - born.year - ((today.month, today.day) < (born.month, born.day))
        except:
            return 0

    def get_maestro_name(self, obj):
        try:
            return obj.maestro.nombre_maestro
        except:
            return ""

    def get_materia_name(self, obj):
        try:
            return obj.materia.nombre_materia
        except:
            return ""

    def get_maestro_json(self, obj):
        try:
            if obj.maestro:
                return {'id': obj.maestro.id, 'text': obj.maestro.nombre_maestro}
        except:
            return {'id': None, 'text': None}

    def get_materia_json(self, obj):
        try:
            if obj.materia:
                return {'id': obj.materia.id, 'text': obj.materia.nombre_materia}
        except:
            return {'id': None, 'text': None}

    class Meta:
        model = Alumno
        fields = ('id',
                  'nombre_alumno',
                  'apellidos_alumno',
                  'fecha_nacimiento_alumno',
                  'materia',
                  'maestro',
                  'disponible',
                  'age',
                  'maestro_name',
                  'materia_name',
                  'maestro_json',
                  'materia_json'
                  )
