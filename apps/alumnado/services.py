# -*- encoding: utf-8 -*-
import datetime

from django.db.models import Q

from apps.alumnado.models import Alumno, Materia, Maestro
from apps.alumnado.serializers import AlumnoSerializers
import json


class AlumnoServices:
    @staticmethod
    def get_list():
        alumnos = Alumno.objects.filter(disponible=True).order_by('id')
        serializer = AlumnoSerializers(alumnos, many=True)
        return serializer.data

    @staticmethod
    def guardar(data):
        try:
            alumno = Alumno.objects.get(pk=data.get('id', 0))
        except Alumno.DoesNotExist as e:
            alumno = Alumno()
        if AlumnoServices.validar_datos(data):
            alumno.nombre_alumno = data.get('nombre_alumno', '')
            alumno.apellidos_alumno = data.get('apellidos_alumno', '')
            fecha_nacimiento_alumno = data.get('fecha_nacimiento_alumno', None)
            fecha_nacimiento_alumno_date = datetime.datetime.strptime(fecha_nacimiento_alumno, "%Y-%m-%d").date()
            if type(fecha_nacimiento_alumno_date) == datetime.date:
                alumno.fecha_nacimiento_alumno = fecha_nacimiento_alumno_date
            else:
                raise Exception("La fecha de nacimiento no puede quedar vacia")
            try:
                materia = Materia.objects.get(pk=data.get('materia',0))
                alumno.materia = materia
            except Materia.DoesNotExist as e:
                raise Exception("La materia no puede quedar vacia")
            try:
                maestro = Maestro.objects.get(pk=data.get('maestro',0))
                alumno.maestro = maestro
            except Materia.DoesNotExist as e:
                raise Exception("El maestro no puede quedar vacio")
            alumno.save()
        return True

    @staticmethod
    def validar_datos(data):
        if data.get('nombre_alumno') == "":
            raise Exception("El nombre del alumno no puede quedar vacio")
        if data.get('apellidos_alumno') == "":
            raise Exception("El apellido del alumno no puede quedar vacio")
        if data.get('fecha_nacimiento_alumno') == "":
            raise Exception("La fecha de nacimiento no puede quedar vacia")
        return True

    @staticmethod
    def inactive(data):
        try:
            alumno = Alumno.objects.get(pk=data.get('id'))
            alumno.disponible = not alumno.disponible
            alumno.save()
            return True
        except Alumno.DoesNotExist as e:
            raise Exception("El alumno no existe")


class MateriaServices:
    @staticmethod
    def get_options(term):
        materias = Materia.objects.filter(Q(nombre_materia__icontains=term)
                                          )[:5]
        # materias = Materia.objects.filter(nombre_materia=term)[:5]
        resultados = []
        for materia in materias:
            resultado = {'id': materia.id, 'text': materia.nombre_materia}
            resultados.append(resultado)
        return json.dumps(resultados)


class MaestroServices:
    @staticmethod
    def get_options(term):
        maestros = Maestro.objects.filter(Q(nombre_maestro__icontains=term)
                                          )[:5]
        respuestas = []
        for maestro in maestros:
            respuesta = {'id': maestro.id, 'text': maestro.nombre_maestro}
            respuestas.append(respuesta)
        return json.dumps(respuestas)
