from django.urls import path, include
from django.views.generic import TemplateView
from apps.alumnado import views as alumnado_views
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path('', alumnado_views.alumnado_vista, name="alumnado_vista"),
    # path('signup/', views.SignUp.as_view(), name='signup'),    
    # path('logout/', views.LogoutView.as_view(next_page='/'), name='logout'),
    path('alumnos/', login_required(alumnado_views.alumno_lista), name="alumno_lista"),
    path('alumnos/editar/<pk>', login_required(alumnado_views.alumno_edit), name="alumno_editar"),
    path('alumnos/delete/<pk>', login_required(alumnado_views.alumno_delete), name="alumno_delete"),
    path('alumno_api', login_required(alumnado_views.AlumnoApi.as_view()), name ="alumno_api"),
    path('materia_search_api', login_required(alumnado_views.MateriaSearchApi.as_view()), name ="materia_search_api"),
    path('maestro_search_api', login_required(alumnado_views.MaestroSearchApi.as_view()), name ="maestro_search_api"),
]
