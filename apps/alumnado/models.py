# -*- encoding: utf-8 -*-
from django.db import models

# Create your models here.
class Materia(models.Model):
    nombre_materia = models.TextField(max_length=100)
    descripcion_materia = models.TextField()

    def __str__(self):
        return self.nombre_materia



class Maestro(models.Model):
    nombre_maestro = models.CharField(max_length=100)
    apellidos_maestro = models.CharField(max_length=100)
    fecha_nacimiento_maestro = models.DateField()
    materia = models.ForeignKey(Materia, on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre_maestro


class Alumno(models.Model):
    nombre_alumno = models.CharField(max_length=100)
    apellidos_alumno = models.CharField(max_length=100)
    fecha_nacimiento_alumno = models.DateField()
    materia = models.ForeignKey(Materia, on_delete=models.CASCADE)
    maestro = models.ForeignKey(Maestro, on_delete=models.CASCADE)
    disponible = models.BooleanField(default = True)

    def __str__(self):
        return self.nombre_alumno