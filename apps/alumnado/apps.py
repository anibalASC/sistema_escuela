from django.apps import AppConfig


class AlumnadoConfig(AppConfig):
    name = 'alumnado'
