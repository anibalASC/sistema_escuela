var Alumnos;
Alumnos = function(){
    var init,
    global_config,
    validar_formulario,
    tabla_alumnos,
    nuevo_alumno,
    guardarDatosAlumnos;


    init = function(config){
        global_config = config;
        validar_formulario();
        tabla_alumnos();
        nuevo_alumno();
    };
    var form = $('#form_new_alumno');
    validar_formulario = function(){
    console.log(form);

        form.validate({
            rules: {
                nombre_alumno: {
                    required: true,
                    minlength: 3,
                    maxlength: 100
                },
                apellidos_alumno: {
                    required: true,
                    minlength: 3,
                    maxlength: 100
                },
                fecha_nacimiento_alumno: {
                    required: true,
                },
                materia: {
                    required: true,
                },
                maestro: {
                    required: true,
                }
            },
            messages: {
                nombre_alumno: {
                    required: "El campo Nombre no puede quedar vacio",
                    minlength: "Minimo de letras es de 3",
                    maxlength: "Maximo de letras es de 100"
                },
                apellidos_alumno: {
                    required: "El campo Apellido no puede quedar vacio",
                    minlength: "Minimo de letras es de 3",
                    maxlength: "Maximo de letras es de 100"
                },
                fecha_de_nacimiento: {
                    required: "Selecciona una Fecha de nacimiento"
                },
                materia: {
                    required: "Selecciona una Materia"
                },
                maestro: {
                    required: "Selecciona un Maestro"
                }
            }
        });
};
    nuevo_alumno = function(){
        $('#button-alumno-new-crear').click(function(){
           guardarDatosAlumnos();
          });
    }
    guardarDatosAlumnos = function () {
    console.log(form.valid());
    console.log(form);

        if (form.valid()){
        $.ajax({
            url: global_config.url_post_nuevo_alumno,
            dataType: "json",
            type: 'post',
            data: $('form#form_new_alumno').serialize()
        }).done(function (response) {
            if (response.success) {
            $('#myModal').modal('hide');
            alert(response.message)
            } else {
                alert(response.error)
            }
        }).always(function () {
            global_config.tabla.ajax.reload();
        });
        }

    };

    tabla_alumnos = function(){
        console.log(global_config)
    };
    return {
        init: init
    }
}