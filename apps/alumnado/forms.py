from django import forms
from .models import Materia, Maestro, Alumno


class MateriaForm(forms.ModelForm):

    class Meta:
        model = Materia
        fields = [
            'nombre_materia',
            'descripcion_materia',
        ]


class MaestroForm(forms.ModelForm):

    class Meta:
        model = Maestro
        fields = [
            'nombre_maestro',
            'apellidos_maestro',
            'fecha_nacimiento_maestro',
            'materia',
        ]


class AlumnoForm(forms.ModelForm):

    class Meta:
        model = Alumno
        template = 'alumnado/alumnado_new.html'
        fields = ('nombre_alumno', 'apellidos_alumno', 
        'fecha_nacimiento_alumno', 'materia', 'maestro',
        )

        widgets = {
            'nombre_alumno': forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Nombre'}),
            'apellidos_alumno': forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Apellido'}),
            'fecha_nacimiento_alumno': forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Fecha de nacimiento'}),
            'materia': forms.Select(attrs={'class': 'form-control'}),
            'maestro': forms.Select(attrs={'class': 'form-control'}),
        }

        help_text = {
            'nombre_alumno': None,
            'apellido_alumno': None,
            'fecha_nacimiento_alumno': None,
            'materia': None,
            'maestro': None,

        }




